/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_SRC_Y_TAB_H_INCLUDED
# define YY_YY_SRC_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    IF = 258,
    ELSE = 259,
    THEN = 260,
    FAILED = 261,
    SET = 262,
    LOGFILE = 263,
    FACILITY = 264,
    DAEMON = 265,
    SYSLOG = 266,
    MAILSERVER = 267,
    HTTPD = 268,
    ALLOW = 269,
    REJECTOPT = 270,
    ADDRESS = 271,
    INIT = 272,
    TERMINAL = 273,
    BATCH = 274,
    READONLY = 275,
    CLEARTEXT = 276,
    MD5HASH = 277,
    SHA1HASH = 278,
    CRYPT = 279,
    DELAY = 280,
    PEMFILE = 281,
    PEMKEY = 282,
    PEMCHAIN = 283,
    ENABLE = 284,
    DISABLE = 285,
    SSLTOKEN = 286,
    CIPHER = 287,
    CLIENTPEMFILE = 288,
    ALLOWSELFCERTIFICATION = 289,
    SELFSIGNED = 290,
    VERIFY = 291,
    CERTIFICATE = 292,
    CACERTIFICATEFILE = 293,
    CACERTIFICATEPATH = 294,
    VALID = 295,
    INTERFACE = 296,
    LINK = 297,
    PACKET = 298,
    BYTEIN = 299,
    BYTEOUT = 300,
    PACKETIN = 301,
    PACKETOUT = 302,
    SPEED = 303,
    SATURATION = 304,
    UPLOAD = 305,
    DOWNLOAD = 306,
    TOTAL = 307,
    UP = 308,
    DOWN = 309,
    IDFILE = 310,
    STATEFILE = 311,
    SEND = 312,
    EXPECT = 313,
    CYCLE = 314,
    COUNT = 315,
    REMINDER = 316,
    REPEAT = 317,
    LIMITS = 318,
    SENDEXPECTBUFFER = 319,
    EXPECTBUFFER = 320,
    FILECONTENTBUFFER = 321,
    HTTPCONTENTBUFFER = 322,
    PROGRAMOUTPUT = 323,
    NETWORKTIMEOUT = 324,
    PROGRAMTIMEOUT = 325,
    STARTTIMEOUT = 326,
    STOPTIMEOUT = 327,
    RESTARTTIMEOUT = 328,
    EXECTIMEOUT = 329,
    PIDFILE = 330,
    START = 331,
    STOP = 332,
    PATHTOK = 333,
    RSAKEY = 334,
    HOST = 335,
    HOSTNAME = 336,
    PORT = 337,
    IPV4 = 338,
    IPV6 = 339,
    TYPE = 340,
    UDP = 341,
    TCP = 342,
    TCPSSL = 343,
    PROTOCOL = 344,
    CONNECTION = 345,
    ALERT = 346,
    NOALERT = 347,
    MAILFORMAT = 348,
    UNIXSOCKET = 349,
    SIGNATURE = 350,
    TIMEOUT = 351,
    RETRY = 352,
    RESTART = 353,
    CHECKSUM = 354,
    EVERY = 355,
    NOTEVERY = 356,
    DEFAULT = 357,
    HTTP = 358,
    HTTPS = 359,
    APACHESTATUS = 360,
    FTP = 361,
    SMTP = 362,
    SMTPS = 363,
    POP = 364,
    POPS = 365,
    IMAP = 366,
    IMAPS = 367,
    CLAMAV = 368,
    NNTP = 369,
    NTP3 = 370,
    MYSQL = 371,
    MYSQLS = 372,
    DNS = 373,
    WEBSOCKET = 374,
    MQTT = 375,
    SSH = 376,
    DWP = 377,
    LDAP2 = 378,
    LDAP3 = 379,
    RDATE = 380,
    RSYNC = 381,
    TNS = 382,
    PGSQL = 383,
    POSTFIXPOLICY = 384,
    SIP = 385,
    LMTP = 386,
    GPS = 387,
    RADIUS = 388,
    MEMCACHE = 389,
    REDIS = 390,
    MONGODB = 391,
    SIEVE = 392,
    SPAMASSASSIN = 393,
    FAIL2BAN = 394,
    STRING = 395,
    PATH = 396,
    MAILADDR = 397,
    MAILFROM = 398,
    MAILREPLYTO = 399,
    MAILSUBJECT = 400,
    MAILBODY = 401,
    SERVICENAME = 402,
    STRINGNAME = 403,
    HOSTGROUPNAME = 404,
    NUMBER = 405,
    PERCENT = 406,
    LOGLIMIT = 407,
    CLOSELIMIT = 408,
    DNSLIMIT = 409,
    KEEPALIVELIMIT = 410,
    REPLYLIMIT = 411,
    REQUESTLIMIT = 412,
    STARTLIMIT = 413,
    WAITLIMIT = 414,
    GRACEFULLIMIT = 415,
    CLEANUPLIMIT = 416,
    REAL = 417,
    CHECKPROC = 418,
    CHECKFILESYS = 419,
    CHECKFILE = 420,
    CHECKDIR = 421,
    CHECKHOST = 422,
    CHECKSYSTEM = 423,
    CHECKFIFO = 424,
    CHECKPROGRAM = 425,
    CHECKNET = 426,
    THREADS = 427,
    CHILDREN = 428,
    METHOD = 429,
    GET = 430,
    HEAD = 431,
    STATUS = 432,
    ORIGIN = 433,
    VERSIONOPT = 434,
    READ = 435,
    WRITE = 436,
    OPERATION = 437,
    SERVICETIME = 438,
    DISK = 439,
    RESOURCE = 440,
    MEMORY = 441,
    TOTALMEMORY = 442,
    LOADAVG1 = 443,
    LOADAVG5 = 444,
    LOADAVG15 = 445,
    SWAP = 446,
    MODE = 447,
    ACTIVE = 448,
    PASSIVE = 449,
    MANUAL = 450,
    ONREBOOT = 451,
    NOSTART = 452,
    LASTSTATE = 453,
    CORE = 454,
    CPU = 455,
    TOTALCPU = 456,
    CPUUSER = 457,
    CPUSYSTEM = 458,
    CPUWAIT = 459,
    CPUNICE = 460,
    CPUHARDIRQ = 461,
    CPUSOFTIRQ = 462,
    CPUSTEAL = 463,
    CPUGUEST = 464,
    CPUGUESTNICE = 465,
    GROUP = 466,
    REQUEST = 467,
    DEPENDS = 468,
    BASEDIR = 469,
    SLOT = 470,
    EVENTQUEUE = 471,
    SECRET = 472,
    HOSTHEADER = 473,
    UID = 474,
    EUID = 475,
    GID = 476,
    MMONIT = 477,
    INSTANCE = 478,
    USERNAME = 479,
    PASSWORD = 480,
    DATABASE = 481,
    TIME = 482,
    ATIME = 483,
    CTIME = 484,
    MTIME = 485,
    CHANGED = 486,
    MILLISECOND = 487,
    SECOND = 488,
    MINUTE = 489,
    HOUR = 490,
    DAY = 491,
    MONTH = 492,
    SSLV2 = 493,
    SSLV3 = 494,
    TLSV1 = 495,
    TLSV11 = 496,
    TLSV12 = 497,
    TLSV13 = 498,
    CERTMD5 = 499,
    AUTO = 500,
    NOSSLV2 = 501,
    NOSSLV3 = 502,
    NOTLSV1 = 503,
    NOTLSV11 = 504,
    NOTLSV12 = 505,
    NOTLSV13 = 506,
    BYTE = 507,
    KILOBYTE = 508,
    MEGABYTE = 509,
    GIGABYTE = 510,
    INODE = 511,
    SPACE = 512,
    TFREE = 513,
    PERMISSION = 514,
    SIZE = 515,
    MATCH = 516,
    NOT = 517,
    IGNORE = 518,
    ACTION = 519,
    UPTIME = 520,
    RESPONSETIME = 521,
    EXEC = 522,
    UNMONITOR = 523,
    PING = 524,
    PING4 = 525,
    PING6 = 526,
    ICMP = 527,
    ICMPECHO = 528,
    NONEXIST = 529,
    EXIST = 530,
    INVALID = 531,
    DATA = 532,
    RECOVERED = 533,
    PASSED = 534,
    SUCCEEDED = 535,
    URL = 536,
    CONTENT = 537,
    PID = 538,
    PPID = 539,
    FSFLAG = 540,
    REGISTER = 541,
    CREDENTIALS = 542,
    URLOBJECT = 543,
    ADDRESSOBJECT = 544,
    TARGET = 545,
    TIMESPEC = 546,
    HTTPHEADER = 547,
    MAXFORWARD = 548,
    FIPS = 549,
    SECURITY = 550,
    ATTRIBUTE = 551,
    FILEDESCRIPTORS = 552,
    HARDLINK = 553,
    GREATER = 554,
    GREATEROREQUAL = 555,
    LESS = 556,
    LESSOREQUAL = 557,
    EQUAL = 558,
    NOTEQUAL = 559
  };
#endif
/* Tokens.  */
#define IF 258
#define ELSE 259
#define THEN 260
#define FAILED 261
#define SET 262
#define LOGFILE 263
#define FACILITY 264
#define DAEMON 265
#define SYSLOG 266
#define MAILSERVER 267
#define HTTPD 268
#define ALLOW 269
#define REJECTOPT 270
#define ADDRESS 271
#define INIT 272
#define TERMINAL 273
#define BATCH 274
#define READONLY 275
#define CLEARTEXT 276
#define MD5HASH 277
#define SHA1HASH 278
#define CRYPT 279
#define DELAY 280
#define PEMFILE 281
#define PEMKEY 282
#define PEMCHAIN 283
#define ENABLE 284
#define DISABLE 285
#define SSLTOKEN 286
#define CIPHER 287
#define CLIENTPEMFILE 288
#define ALLOWSELFCERTIFICATION 289
#define SELFSIGNED 290
#define VERIFY 291
#define CERTIFICATE 292
#define CACERTIFICATEFILE 293
#define CACERTIFICATEPATH 294
#define VALID 295
#define INTERFACE 296
#define LINK 297
#define PACKET 298
#define BYTEIN 299
#define BYTEOUT 300
#define PACKETIN 301
#define PACKETOUT 302
#define SPEED 303
#define SATURATION 304
#define UPLOAD 305
#define DOWNLOAD 306
#define TOTAL 307
#define UP 308
#define DOWN 309
#define IDFILE 310
#define STATEFILE 311
#define SEND 312
#define EXPECT 313
#define CYCLE 314
#define COUNT 315
#define REMINDER 316
#define REPEAT 317
#define LIMITS 318
#define SENDEXPECTBUFFER 319
#define EXPECTBUFFER 320
#define FILECONTENTBUFFER 321
#define HTTPCONTENTBUFFER 322
#define PROGRAMOUTPUT 323
#define NETWORKTIMEOUT 324
#define PROGRAMTIMEOUT 325
#define STARTTIMEOUT 326
#define STOPTIMEOUT 327
#define RESTARTTIMEOUT 328
#define EXECTIMEOUT 329
#define PIDFILE 330
#define START 331
#define STOP 332
#define PATHTOK 333
#define RSAKEY 334
#define HOST 335
#define HOSTNAME 336
#define PORT 337
#define IPV4 338
#define IPV6 339
#define TYPE 340
#define UDP 341
#define TCP 342
#define TCPSSL 343
#define PROTOCOL 344
#define CONNECTION 345
#define ALERT 346
#define NOALERT 347
#define MAILFORMAT 348
#define UNIXSOCKET 349
#define SIGNATURE 350
#define TIMEOUT 351
#define RETRY 352
#define RESTART 353
#define CHECKSUM 354
#define EVERY 355
#define NOTEVERY 356
#define DEFAULT 357
#define HTTP 358
#define HTTPS 359
#define APACHESTATUS 360
#define FTP 361
#define SMTP 362
#define SMTPS 363
#define POP 364
#define POPS 365
#define IMAP 366
#define IMAPS 367
#define CLAMAV 368
#define NNTP 369
#define NTP3 370
#define MYSQL 371
#define MYSQLS 372
#define DNS 373
#define WEBSOCKET 374
#define MQTT 375
#define SSH 376
#define DWP 377
#define LDAP2 378
#define LDAP3 379
#define RDATE 380
#define RSYNC 381
#define TNS 382
#define PGSQL 383
#define POSTFIXPOLICY 384
#define SIP 385
#define LMTP 386
#define GPS 387
#define RADIUS 388
#define MEMCACHE 389
#define REDIS 390
#define MONGODB 391
#define SIEVE 392
#define SPAMASSASSIN 393
#define FAIL2BAN 394
#define STRING 395
#define PATH 396
#define MAILADDR 397
#define MAILFROM 398
#define MAILREPLYTO 399
#define MAILSUBJECT 400
#define MAILBODY 401
#define SERVICENAME 402
#define STRINGNAME 403
#define HOSTGROUPNAME 404
#define NUMBER 405
#define PERCENT 406
#define LOGLIMIT 407
#define CLOSELIMIT 408
#define DNSLIMIT 409
#define KEEPALIVELIMIT 410
#define REPLYLIMIT 411
#define REQUESTLIMIT 412
#define STARTLIMIT 413
#define WAITLIMIT 414
#define GRACEFULLIMIT 415
#define CLEANUPLIMIT 416
#define REAL 417
#define CHECKPROC 418
#define CHECKFILESYS 419
#define CHECKFILE 420
#define CHECKDIR 421
#define CHECKHOST 422
#define CHECKSYSTEM 423
#define CHECKFIFO 424
#define CHECKPROGRAM 425
#define CHECKNET 426
#define THREADS 427
#define CHILDREN 428
#define METHOD 429
#define GET 430
#define HEAD 431
#define STATUS 432
#define ORIGIN 433
#define VERSIONOPT 434
#define READ 435
#define WRITE 436
#define OPERATION 437
#define SERVICETIME 438
#define DISK 439
#define RESOURCE 440
#define MEMORY 441
#define TOTALMEMORY 442
#define LOADAVG1 443
#define LOADAVG5 444
#define LOADAVG15 445
#define SWAP 446
#define MODE 447
#define ACTIVE 448
#define PASSIVE 449
#define MANUAL 450
#define ONREBOOT 451
#define NOSTART 452
#define LASTSTATE 453
#define CORE 454
#define CPU 455
#define TOTALCPU 456
#define CPUUSER 457
#define CPUSYSTEM 458
#define CPUWAIT 459
#define CPUNICE 460
#define CPUHARDIRQ 461
#define CPUSOFTIRQ 462
#define CPUSTEAL 463
#define CPUGUEST 464
#define CPUGUESTNICE 465
#define GROUP 466
#define REQUEST 467
#define DEPENDS 468
#define BASEDIR 469
#define SLOT 470
#define EVENTQUEUE 471
#define SECRET 472
#define HOSTHEADER 473
#define UID 474
#define EUID 475
#define GID 476
#define MMONIT 477
#define INSTANCE 478
#define USERNAME 479
#define PASSWORD 480
#define DATABASE 481
#define TIME 482
#define ATIME 483
#define CTIME 484
#define MTIME 485
#define CHANGED 486
#define MILLISECOND 487
#define SECOND 488
#define MINUTE 489
#define HOUR 490
#define DAY 491
#define MONTH 492
#define SSLV2 493
#define SSLV3 494
#define TLSV1 495
#define TLSV11 496
#define TLSV12 497
#define TLSV13 498
#define CERTMD5 499
#define AUTO 500
#define NOSSLV2 501
#define NOSSLV3 502
#define NOTLSV1 503
#define NOTLSV11 504
#define NOTLSV12 505
#define NOTLSV13 506
#define BYTE 507
#define KILOBYTE 508
#define MEGABYTE 509
#define GIGABYTE 510
#define INODE 511
#define SPACE 512
#define TFREE 513
#define PERMISSION 514
#define SIZE 515
#define MATCH 516
#define NOT 517
#define IGNORE 518
#define ACTION 519
#define UPTIME 520
#define RESPONSETIME 521
#define EXEC 522
#define UNMONITOR 523
#define PING 524
#define PING4 525
#define PING6 526
#define ICMP 527
#define ICMPECHO 528
#define NONEXIST 529
#define EXIST 530
#define INVALID 531
#define DATA 532
#define RECOVERED 533
#define PASSED 534
#define SUCCEEDED 535
#define URL 536
#define CONTENT 537
#define PID 538
#define PPID 539
#define FSFLAG 540
#define REGISTER 541
#define CREDENTIALS 542
#define URLOBJECT 543
#define ADDRESSOBJECT 544
#define TARGET 545
#define TIMESPEC 546
#define HTTPHEADER 547
#define MAXFORWARD 548
#define FIPS 549
#define SECURITY 550
#define ATTRIBUTE 551
#define FILEDESCRIPTORS 552
#define HARDLINK 553
#define GREATER 554
#define GREATEROREQUAL 555
#define LESS 556
#define LESSOREQUAL 557
#define EQUAL 558
#define NOTEQUAL 559

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 335 "src/p.y" /* yacc.c:1909  */

        URL_T url;
        Address_T address;
        float real;
        int   number;
        char *string;

#line 670 "src/y.tab.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_SRC_Y_TAB_H_INCLUDED  */
